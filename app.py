from dboperations import DbOperations

class Server(object):
    
    # take user input and perform the action what user wants to do
    def userWants(self):
        
        # initialise a boolean variable with true to identify is user is interested continue or not
        # initial its true to show options and take action
        # if user userWantsToContinue is false than function exits
        userWantsToContinue = True
        
        while userWantsToContinue:
            # showing the list of operations     
            print("1. Top n Countries/Cities/zip-codes where reservations came from and the number of reservations and room nights associated with each.")
            print("2. Top n block reservations and the number of room nights booked")
            print("3. List all reservations that are not valid (Room type not in valid list or Rate Category not in valid list)")
            print("4. Top 10 non block customers (where the sum of revenue + package revenue is the maximum)")
            print("5. Top n Companies")
            print("6. Count all reservation that are in a given rate category.  Give both count, sum(revenue) per each rate category.")
            
            try:
                # userResponse is integer value to take user input, to take action on the operation they specified.
                # the numbers are the operations serially

                userResponse = int(input("Your choice? "))
                if userResponse == 1:
                    
                    try:
                        self.dbOperationsObj.questionFirst()
                    
                    except:
                        print("Error while making query 1...!")
                
                elif userResponse == 2:
                    
                    try:
                        self.dbOperationsObj.dataOperations()              
                    
                    except:
                        print("Error while making query 2 ...!")
                
                elif userResponse == 3:
                    
                    try:
                        self.dbOperationsObj.questionThird() 
                    
                    except:
                        print("Error while making query 3 ...!")
                
                elif userResponse == 4:
                    
                    try:
                        self.dbOperationsObj.questionFourth() 
                    
                    except:
                        print("Error while making query 4 ..!")

                elif userResponse == 5:
                
                    try:
                        self.dbOperationsObj.questionFifth() 
                    except:
                        print("Error while making query 5 ..!")

                elif userResponse == 6:
                    
                    try:
                        self.dbOperationsObj.questionSixth() 
                    except:
                        print("Error while making query 6 ...!")
                else:
                    # if user inputs the non-integer its shows the message
                    print("\n Please select valid option...!\n")
            
            except:
                # triggers when there is error in database or if user enters the non-integer
                print("\nPlease enter valid input\n")
           
            # ask user is he/she interested to continue if user input is n or N exits the function
            user = input("\nWants to continue? (y/N): ")
           
            # check is user entered n or N 
            if user.lower() == 'n':
                # userWantsToContinue boolean variable so while loop will not enter inside
                userWantsToContinue = False

    # class constructor
    def __init__(self):

        # creating object for DbOperations class which resides in dboperations.py
        # this object is used to make operations
        self.dbOperationsObj = DbOperations()

        # initial step to ask user want he/she wants
        # lists operations which user can perform 
        self.userWants()

if __name__ == "__main__":
    # initialise object
    obj = Server()
