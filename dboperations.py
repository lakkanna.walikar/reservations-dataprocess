import psycopg2
from dbconfig import DbConfig
class DbOperations():
    # creating object for DbConfig class its initialises the connection to the database
    dbConfigureObj = DbConfig()

    # call the dbConnect function its initialises the connection to the database and returns the cursor 
    cursor = dbConfigureObj.dbConnect()
    
    # function for query 1
    def questionFirst(self):
        print("\nfirst operation\n")

    # function for query 2
    def dataOperations(self):
        #self.cursor = cursor

        self.cursor.execute("select reservationid, sum(nights) as nights_booked, totalrevenue from reservationdata where blockid=0 group by reservationid order by totalrevenue desc limit 10;")
        results = self.cursor.fetchall()
        print("{:10}\t{:10}\t{:10}".format("Reservation ID","Nights Booked","Total Revenue"))
        print('')
        for result in results:
            print("{:10}\t{:10}\t{:10}".format(result[0], result[1], result[2]))
            #print(key,value)


    # function for query 3 
    def questionThird(self):
        print("\nthird operation\n")

    # function for query 4 
    def questionFourth(self):
        print("\nfourth operation\n")
        
        # function for query 5 
    def questionFifth(self):
        print("\nfifth operation\n")

        # function for query 6 
    def questionSixth(self):
        print("\nsixth operation\n")

#            print("\nOops!! something went wrong to get data...!\n")
#if __name__ == "__main__":
#    obj = DbOperations()
#    obj.questionFirst()
#    obj.dataOperations()
